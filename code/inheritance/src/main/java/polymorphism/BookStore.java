package polymorphism;

public class BookStore {
    public static void main(String[] args)
    {

        Book[] books = new Book[5];
        books[0] = new Book("Sherlock Holmes", "Arthur Conan Doyle");
        books[2] = new Book("Caillou", "Caillou's author");
        
        books[1] = new ElectronicBook("Algebra", "Miss K", 50);
        books[3] = new ElectronicBook("Programming", "Mr. Pomerantz", 60);
        books[4] = new ElectronicBook("Databse", "Miss Sadeghi", 70);

        for (int i = 0; i < books.length; i++)
        {
            System.out.println(books[i]);
        }

        ElectronicBook b = (ElectronicBook) books[0];
        System.out.println(b.getNumberBytes());

    }
}
